const components = require('./components')

const cleanup = require('../server/services/cleanup.cron')

module.exports = {
  components,
  db: {},
  port: 3000,
  protocol: 'http',
  useGraphQLServer: false,
  pool: { min: 0, max: 10, idleTimeoutMillis: 1000 },

  onStartup: [
    {
      label: 'Create client runner through environment',
      execute: async () => {
        const {
          createServiceClientFromEnvironment,
          /* eslint-disable-next-line global-require */
        } = require('@coko/service-auth')

        await createServiceClientFromEnvironment()
      },
    },
  ],

  jobQueues: [
    {
      name: 'cleanup-static',
      handler: cleanup,
      schedule: '*/5 * * * *',
    },
  ],

  teams: {
    global: [],
    nonGlobal: [],
  },

  devServerIgnore: ['./static'],
}
