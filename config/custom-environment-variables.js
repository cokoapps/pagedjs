module.exports = {
  secret: 'SECRET',
  db: {
    host: 'POSTGRES_HOST',
    port: 'POSTGRES_PORT',
    database: 'POSTGRES_DB',
    user: 'POSTGRES_USER',
    password: 'POSTGRES_PASSWORD',
    allowSelfSignedCertificates: {
      __name: 'POSTGRES_ALLOW_SELF_SIGNED_CERTIFICATES',
      __format: 'json',
    },
    caCert: 'POSTGRES_CA_CERT',
  },

  port: 'SERVER_PORT',
  serverUrl: 'SERVER_URL',

  clientID: 'CLIENT_ID',
  clientSecret: 'CLIENT_SECRET',
}
