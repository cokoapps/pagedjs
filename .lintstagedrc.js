const { lintstaged } = require('@coko/lint')

lintstaged['*.js'] = lintstaged['*.js'].filter(i => i !== 'stylelint')

module.exports = lintstaged
