FROM node:20.18.0-alpine3.19

# Configuration for GS4JS
ENV GS4JS_HOME=/usr/lib
ENV CONNECTION_TIMEOUT=60000

RUN apk update && apk add --no-cache \
    bash \
    ca-certificates \
    chromium \
    coreutils \
    dumb-init \
    freetype \
    freetype-dev \
    g++ \
    gcc \
    ghostscript \
    ghostscript-dev \
    git \
    harfbuzz \
    make \
    nss \
    python3 \
    ttf-freefont \
    unzip

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
    PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser

RUN mkdir -p /home/node/Downloads

WORKDIR /home/node/pagedjs

RUN corepack enable
RUN chown -R node:node /home/node/pagedjs

USER node

COPY --chown=node:node .yarnrc.yml .
COPY --chown=node:node package.json ./package.json
COPY --chown=node:node yarn.lock ./yarn.lock

RUN yarn workspaces focus --production && yarn cache clean && rm -rf ~/.npm

COPY --chown=node:node . .

CMD ["yarn", "coko-server", "start"]
