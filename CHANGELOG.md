## [2.0.7](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v2.0.6...v2.0.7) (2025-02-04)


### Bug Fixes

* fix postgres ssl connections ([2508e9b](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/2508e9b26f08019793e7904ceb551f4ba200bc87))

## [2.0.6](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v2.0.5...v2.0.6) (2024-12-05)

## [2.0.5](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v2.0.4...v2.0.5) (2024-12-05)


### Bug Fixes

* remove yarn and npm cache from containers ([7da2f1c](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/7da2f1c5ab03f57a6e04a788ca19eb00c30f0c2b))

## [2.0.4](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v2.0.3...v2.0.4) (2024-12-05)


### Bug Fixes

* track ghostscript4js in dependencies ([3b2f120](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/3b2f1209f0d00fdf808ae59593f8a262fb08ee8a))

## [2.0.3](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v2.0.2...v2.0.3) (2024-12-05)


### Bug Fixes

* do not install dev dependencies in production ([1f3e332](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/1f3e33227b56972adaf2675e3275ba08d51038fd))
* fix production dependencies install ([c9808b8](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/c9808b8c031d207d630d2734509bfd5403e723fd))

## [2.0.3](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v2.0.2...v2.0.3) (2024-12-05)


### Bug Fixes

* do not install dev dependencies in production ([1f3e332](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/1f3e33227b56972adaf2675e3275ba08d51038fd))

## [2.0.2](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v2.0.1...v2.0.2) (2024-12-02)


### Bug Fixes

* bump service-auth version to exit create:client script ([1703bbf](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/1703bbfe7da3da1696c6cc203132267d35a78916))

## [2.0.1](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v2.0.0...v2.0.1) (2024-10-24)


### Bug Fixes

* Content-Security-Policy for embedding in iframes ([59dbd87](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/59dbd8784ca54e2a5d01eabe819f2182d46af89a))

# [2.0.0](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.6.10...v2.0.0) (2024-10-15)


### chore

* move to yarn v4 ([25fe615](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/25fe615639a57469cebc1d337dd2446616abca9e))


### BREAKING CHANGES

* Renamed environment variables: `PUBLIC_URL` to `SERVER_URL`, `PUBSWEET_SECRET` to `SECRET`

### [1.6.10](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.6.9...v1.6.10) (2024-01-31)

### [1.6.9](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.6.8...v1.6.9) (2023-09-11)

### [1.6.8](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.6.7...v1.6.8) (2023-08-21)

### Bug Fixes

- added missing return ([e9f2dbc](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/e9f2dbc48035f6997fc90c241c3996118a11a272))

### [1.6.7](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.6.6...v1.6.7) (2023-08-21)

### [1.6.6](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.6.4...v1.6.6) (2023-08-21)

### Bug Fixes

- **service:** added forgotten await keywords ([c883844](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/c8838442bc3e4dc03fbcc7a8e9faf861548fa068))
- **service:** fix zoom issues and update ([4f1e090](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/4f1e0900f6b68e6349c055520a694216456fd6b4))

### [1.6.4](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.6.3...v1.6.4) (2023-07-11)

### Bug Fixes

- **service:** add more rules in interface css ([fab9d8a](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/fab9d8abe0b73760f58bbc96d01149dfc6cade15))

### [1.6.3](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.6.2...v1.6.3) (2023-07-11)

### Bug Fixes

- **service:** change from zoom to scale, changes in interface css ([af638b3](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/af638b386137e428918ac81a1542674588d6262f))

### [1.6.2](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.6.1...v1.6.2) (2023-07-10)

### Bug Fixes

- **service:** added zoom support ([74b6364](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/74b6364408b9bb20ffbf72d82cfae06af89f641c))

### [1.6.1](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.6.0...v1.6.1) (2023-07-10)

### Bug Fixes

- **service:** id added for the case of preview ([e33b9f2](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/e33b9f2abda36e05a9ee83d7c36e6a6b826e7ac7))

## [1.6.0](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.5.0...v1.6.0) (2023-07-07)

### Features

- **service:** interface css added for controlling page spread and bckground ([f7b63f8](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/f7b63f8a557cfd6053efb74e8650f4559a615ec7))

## [1.5.0](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.4.7...v1.5.0) (2023-03-17)

### Features

- update pagedjs dependencies ([118d2c1](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/118d2c10f13db8e48dba1fd7819933df2f645669))

### [1.4.7](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.4.4...v1.4.7) (2022-11-17)

### Bug Fixes

- **service:** return to prev ver of pagedjs ([5a24c09](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/5a24c09bac8ef2544f29c2a975ab91acc5327798))
- **service:** wait script ([17df3e0](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/17df3e0185c0c5c53a85e6fe953eec664c93b029))

### [1.4.4](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.4.3...v1.4.4) (2022-07-15)

### Bug Fixes

- **service:** remove highlight css ([8eb4e74](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/8eb4e74e5f2411460d77116997bfd0a7e61ed9a2))

### [1.4.3](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.4.2...v1.4.3) (2022-07-15)

### [1.4.2](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.4.1...v1.4.2) (2022-07-12)

### Bug Fixes

- add missing bash ([8fa6574](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/8fa657424261e3a40e584a03f4ba54dba4aad042))

### [1.4.1](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.4.0...v1.4.1) (2022-07-12)

## [1.4.0](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.3.1...v1.4.0) (2021-11-30)

### Features

- **service:** info endpoint ([81760b2](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/81760b25bf3b142f0563fe987b78da28cb068f72))

### [1.3.1](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.3.0...v1.3.1) (2021-11-25)

### Bug Fixes

- **service:** added python ([d6bb3fc](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/d6bb3fcc21a4b8a6118ab669b7723219e3cffd21))

## [1.3.0](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.2.0...v1.3.0) (2021-11-25)

### Features

- **service:** support kotahi ([5e0dce4](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/5e0dce497065971129940744378cd8ecc93d4245))

### Bug Fixes

- **service:** fixed footnotes for PDF ([945627f](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/945627f73e078ad80ddac44c8623a4adab80271b))

## [1.2.0](https://gitlab.coko.foundation/cokoapps/pagedjs/compare/v1.1.0...v1.2.0) (2021-05-31)

### Features

- **service:** support of export scripts added ([80ddb93](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/80ddb9355ffb633aa1193f38482bcff9385d975e))

## 1.1.0 (2021-04-15)

### Features

- **service:** clean-up cron task added ([b06fe31](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/b06fe31e75b3e54a8b2e316cd412224e8438a42d))
- **service:** fetching images ([8055eab](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/8055eab13d6d3ae464393ef07f7a35a0da26394a))
- **service:** generating pdf finalized ([1697102](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/16971026109f42b4f5a90cd81e379bde5f40574f))
- **service:** init of service ([e51334a](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/e51334aa6cf29c714cebfaa0a6b671e44276a203))
- **service:** previewer feature added ([6a53508](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/6a5350804e8ebd53ed4001991d6e805a85168186))

### Bug Fixes

- **service:** ensure dirs ([83ddbee](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/83ddbee7056b9b50e984a28d299048ba4c2f6d19))
- **service:** external url when proxy ([3588cbf](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/3588cbfa36afa991ab45fd227c0fbc0b189e99f7))
- **service:** forgotten dep ([b0f7c2a](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/b0f7c2a3fcf77259c228035338670b2bb707516a))
- **service:** static index not ignored ([dfa9524](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/dfa95240a26e5561ca32a930cbe3520e67f933de))
- **service:** x-frame-options header removed ([2775f5e](https://gitlab.coko.foundation/cokoapps/pagedjs/commit/2775f5e6f8b3084b7680fdccdbf2224eb42ac32a))
