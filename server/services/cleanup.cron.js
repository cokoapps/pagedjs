const fs = require('fs-extra')
const path = require('path')

// Clean-up static folder every 5 minutes and delete folders created 10 minutes ago
const cleanup = () => {
  /* eslint-disable-next-line no-console */
  console.info('Cleaning up static folder...')

  try {
    fs.readdir(
      `${path.join(__dirname, '..', 'static')}`,
      async (err, files) => {
        if (err) throw err

        await Promise.all(
          files.map(async file => {
            if (
              fs
                .lstatSync(
                  path.resolve(`${path.join(__dirname, '..', 'static')}`, file),
                )
                .isDirectory()
            ) {
              // const EIGHTHOURS = 1000 * 60 * 60 * 8
              const TEN_MINUTES = 1000 * 60 * 10
              // const eightHoursAgo = new Date().getTime() - EIGHTHOURS
              const tenMinutesAgo = new Date().getTime() - TEN_MINUTES

              if (file !== 'common-stylesheets' && file <= tenMinutesAgo) {
                return fs.remove(path.join(__dirname, '..', 'static', file))
              }
            }

            return false
          }),
        )
      },
    )
  } catch (e) {
    throw new Error(e)
  }
}

module.exports = cleanup
